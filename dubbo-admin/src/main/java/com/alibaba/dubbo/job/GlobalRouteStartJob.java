package com.alibaba.dubbo.job;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.dubbo.common.logger.Logger;
import com.alibaba.dubbo.common.logger.LoggerFactory;
import com.alibaba.dubbo.governance.service.ProviderService;
import com.alibaba.dubbo.governance.service.RouteService;
import com.alibaba.dubbo.registry.common.domain.Route;

public class GlobalRouteStartJob {

	private static final Logger logger = LoggerFactory.getLogger(GlobalRouteStartJob.class);
	
	@Autowired
	private RouteService routeService;

	@Autowired
	private ProviderService providerService;

	public void execute() {
		try {
			List<Route> globalRoutes = routeService.findGlobalRoutes();

			for (Route route : globalRoutes) {
				try {
					if (route.isAutoStartup()) {
						Long id = route.getId();
						boolean isEnabled = route.isEnabled();

						if (id != null && !isEnabled) {
							routeService.enableRoute(id);
						}
					}
				} catch (Exception ex) {
				}
			}
		} catch (Exception ex) {
			logger.error("GlobalRouteStartJob execute error!");
		}
	}

}
